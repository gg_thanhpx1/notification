# notification

## Get config info of Firebase
Access to the control panel and select Firebase project >> Project Overview >> Project Settings  

- API: In Service accounts tab >> Click to button "Generate new private key" to download generic Firebase file  

- FE: Need 2 fields: Web API key (In General tab) and Sender ID (In Cloud Messaging tab) 

- Android and iOS: Config in General tab  


## How to run project

1. Create a db in MySql with name is notification

2. Change the account to access your mysql in the application-dev.yml (Dev profile), application-stag.yml (Staging profile), application-live.yml (Live profile)

3. Change path to the current file location of the generic Firebase file in field service-account-file in application-xxx.yml

4. Run project with mvn
```
   mvn spring-boot:run -Pdev
```

## The main function

```
	private void guiNotiDenDevice(DeviceDTO device, String url, String noiDung, Map<String, String> data) throws InterruptedException {
	    // Put data into message and set topic for sending message
	    System.out.println("Gui toi device: " + device.getId() + " " + device.getToken());
        String clickActionUrl = appUrl + url;
       
        if (device.getToken() != null) {
            try {
                Message mess = null;
                if (LoaiThietBiEnum.WEB.name().equalsIgnoreCase(device.getLoaiThietBi())) {
                    // Push noti cho Web
                    System.out.println("gui noti cho web");
                    WebpushNotification webpushNotification = WebpushNotification.builder()
                            .setBody(noiDung)
                            .setTitle("Bạn có một thông báo mới")
                            .setIcon("http://qoffice-staging.quangnam.gov.vn/logo-eoffice.svg")
                            .putCustomData("click_action", clickActionUrl)
                            .build();
                    
                    WebpushConfig webpushConfig = WebpushConfig.builder()                                
                            .putHeader("ttl", "300")
                            .setNotification(webpushNotification)
                            .build();
                    
                    mess = Message.builder().putAllData(data)
                            .setToken(device.getToken())
                            .setWebpushConfig(webpushConfig)
                            .build();
                } else if (LoaiThietBiEnum.ANDROID.name().equalsIgnoreCase(device.getLoaiThietBi())) {
                    // Push noti cho Android
                    System.out.println("gui noti cho android");
                    AndroidNotification androidNotification = AndroidNotification.builder()
                            .setTitle("Bạn có một thông báo mới")
                            .setBody(noiDung)
                            .setIcon("http://qoffice-staging.quangnam.gov.vn/logo-eoffice.svg")
                            .setColor("#f45342")
                            .build();
                    
                    AndroidConfig androidConfig = AndroidConfig.builder()
                            .setTtl(3600 * 1000) // 1 hour in milliseconds
                            .setPriority(AndroidConfig.Priority.NORMAL)
                            .setNotification(androidNotification)
                            .build();
                    
                    mess = Message.builder().putAllData(data)
                            .setToken(device.getToken())
                            .setAndroidConfig(androidConfig)
                            .build();
                } else if (LoaiThietBiEnum.IOS.name().equalsIgnoreCase(device.getLoaiThietBi())) {
                    // Push noti cho iOS
                    System.out.println("gui noti cho ios");
                    ApsAlert apsAlert = ApsAlert.builder()
                            .setTitle("Bạn có một thông báo mới")
                            .setBody(noiDung)
                            .build();
                    
                    Aps aps = Aps.builder()
                            .setBadge(42)
                            .setAlert(apsAlert)
                            .build();
                    
                    ApnsConfig apnsConfig = ApnsConfig.builder()
                            .putHeader("apns-priority", "10")
                            .setAps(aps)
                            .build();
                    mess = Message.builder().putAllData(data)
                            .setToken(device.getToken())
                            .setApnsConfig(apnsConfig)
                            .build();
                }                
                if (mess != null) {
                    try {
                        // Get response from FCM Service
                        String response = FirebaseMessaging.getInstance().sendAsync(mess).get();
                        System.out.println("Sent message: " + response);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IllegalArgumentException e) {
               System.out.println("==========token khong dung: " + device.getToken());
            }
        }   
	}
```
