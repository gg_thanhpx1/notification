package vn.greenglobal.service;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.ApsAlert;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.WebpushConfig;
import com.google.firebase.messaging.WebpushNotification;

import vn.greenglobal.dto.DeviceDTO;
import vn.greenglobal.dto.NotificationDTO;
import vn.greenglobal.dto.ThongTinUrlDto;
import vn.greenglobal.enums.LoaiThietBiEnum;
import vn.greenglobal.enums.NotificationsStatusEnum;
import vn.greenglobal.model.request.CreateThongTinUrlRequest;
import vn.greenglobal.model.request.NotificationRequest;
import vn.greenglobal.model.request.NotificationWithParamRequest;
import vn.greenglobal.model.request.RegisterDeviceRequest;
import vn.greenglobal.model.request.UnregisterDeviceRequest;
import vn.greenglobal.model.request.UpdateDeviceRequest;
import vn.greenglobal.model.request.UpdateThongTinUrlRequest;
import vn.greenglobal.service.command.DeviceCmdHandler;
import vn.greenglobal.service.command.NotificationCmdHandler;
import vn.greenglobal.service.command.NotificationNhanVienCmdHandler;
import vn.greenglobal.service.command.ThongTinUrlCmdHandler;
import vn.greenglobal.service.command.dto.device.CreateDeviceCmd;
import vn.greenglobal.service.command.dto.device.DeleteDeviceCmd;
import vn.greenglobal.service.command.dto.device.UpdateDeviceCmd;
import vn.greenglobal.service.command.dto.notification.CreateNotificationCmd;
import vn.greenglobal.service.command.dto.notification_nhanvien.CreateNotificationNhanVienCmd;
import vn.greenglobal.service.command.dto.notification_nhanvien.UpdateDaDocNotificationNhanVienCmd;
import vn.greenglobal.service.command.dto.thongtinurl.CreateThongTinUrlCmd;
import vn.greenglobal.service.command.dto.thongtinurl.DeleteThongTinUrlCmd;
import vn.greenglobal.service.command.dto.thongtinurl.UpdateThongTinUrlCmd;
import vn.greenglobal.service.query.DeviceQueryHandler;
import vn.greenglobal.service.query.NotificationQueryHandler;
import vn.greenglobal.service.query.ThongTinUrlQueryHandler;
import vn.greenglobal.service.query.dto.device.GetDsDeviceQuery;
import vn.greenglobal.service.query.dto.notification.GetDsNotificationQuery;
import vn.greenglobal.service.query.dto.thongtinurl.GetDetailThongTinUrlByKeyQuery;
import vn.greenglobal.service.query.dto.thongtinurl.GetDsThongTinUrlQuery;

@Service
public class PushNotificationServiceImpl implements PushNotificationService {

	@Autowired
	private NotificationCmdHandler notificationCmdHandler;

	@Autowired
	private NotificationQueryHandler notificationQueryHandler;

	@Autowired
	private NotificationNhanVienCmdHandler notificationNhanVienCmdHandler;

	@Autowired
	private DeviceCmdHandler deviceCmdHandler;

	@Autowired
	private DeviceQueryHandler deviceQueryHandler;
	
	@Autowired
    private ThongTinUrlCmdHandler thongTinUrlCmdHandler;
	
	@Autowired
    private ThongTinUrlQueryHandler thongTinUrlQueryHandler;
	
	@Value("${app-url}")
	private String appUrl;
	
	@Value("${service-account-file}")
    private String serviceAccountFile;

	private Logger logger = LoggerFactory.getLogger(PushNotificationServiceImpl.class);

	public PushNotificationServiceImpl() {
	    
	}
	
	@PostConstruct
    public void init() {
        try (InputStream serviceAccount = getClass().getClassLoader().getResourceAsStream(serviceAccountFile)) {
            logger.info("Service Account " + serviceAccount.toString());

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount)).build();

            boolean hasBeenInitialized=false;
            List<FirebaseApp> firebaseApps = FirebaseApp.getApps();
            for(FirebaseApp app : firebaseApps){
                if(app.getName().equals(FirebaseApp.DEFAULT_APP_NAME)){
                    hasBeenInitialized=true;
                }
            }
            if (!hasBeenInitialized) {
                FirebaseApp.initializeApp(options);
            }
            
        } catch (IOException e) {
            logger.error(e.toString());
            e.printStackTrace();
        }
    }

	@Override
	public void sendNotificationToClient(NotificationRequest request) {
		try {
			// Add a new record to Notification table
			CreateNotificationCmd nCmd = new CreateNotificationCmd();
			nCmd.setNoiDung(request.getNoiDung());
			nCmd.setUrl(request.getUrl());

			notificationCmdHandler.handle(nCmd);

			// Convert json string to list of long variable
			List<Long> list = request.getNguoiNhanIds().stream().distinct().collect(Collectors.toList());

			// Add records to NotificationNhanVien table
			for (int i = 0; i < list.size(); i++) {
				CreateNotificationNhanVienCmd nnCmd = new CreateNotificationNhanVienCmd();
				nnCmd.setNotificationId(nCmd.getId());
				nnCmd.setChucDanhId(list.get(i));
				nnCmd.setDaDoc(0);

				notificationNhanVienCmdHandler.handle(nnCmd);
			}

			// Get list of device of client
			List<DeviceDTO> devices = new ArrayList<DeviceDTO>();
			for (int i = 0; i < list.size(); i++) {
				GetDsDeviceQuery query = new GetDsDeviceQuery();
				query.setChucDanhId(list.get(i));

				List<DeviceDTO> iDevices = deviceQueryHandler.handle(query);
				if (iDevices != null) {
					devices.addAll(iDevices);
				}
			}

			// Convert object to json object for adding on a request FCM
			Map<String, String> data = new HashMap<>();
			data.put("noiDung", request.getNoiDung());
			data.put("url", request.getUrl());
			data.put("id", nCmd.getId().toString());
			
			for (int i = 0; i < devices.size(); i++) {
			    guiNotiDenDevice(devices.get(i), request.getUrl(), request.getNoiDung(), data);				
			}
		} catch (Exception ex) {
		    ex.printStackTrace();
			logger.error(ex.toString());
		}
	}
	
	private void guiNotiDenDevice(DeviceDTO device, String url, String noiDung, Map<String, String> data) throws InterruptedException {
	    // Put data into message and set topic for sending message
	    System.out.println("Gui toi device: " + device.getId() + " " + device.getToken());
        String clickActionUrl = appUrl + url;
       
        if (device.getToken() != null) {
            try {
                Message mess = null;
                if (LoaiThietBiEnum.WEB.name().equalsIgnoreCase(device.getLoaiThietBi())) {
                    // Push noti cho Web
                    System.out.println("gui noti cho web");
                    WebpushNotification webpushNotification = WebpushNotification.builder()
                            .setBody(noiDung)
                            .setTitle("Bạn có một thông báo mới")
                            .setIcon("http://qoffice-staging.quangnam.gov.vn/logo-eoffice.svg")
                            .putCustomData("click_action", clickActionUrl)
                            .build();
                    
                    WebpushConfig webpushConfig = WebpushConfig.builder()                                
                            .putHeader("ttl", "300")
                            .setNotification(webpushNotification)
                            .build();
                    
                    mess = Message.builder().putAllData(data)
                            .setToken(device.getToken())
                            .setWebpushConfig(webpushConfig)
                            .build();
                } else if (LoaiThietBiEnum.ANDROID.name().equalsIgnoreCase(device.getLoaiThietBi())) {
                    // Push noti cho Android
                    System.out.println("gui noti cho android");
                    AndroidNotification androidNotification = AndroidNotification.builder()
                            .setTitle("Bạn có một thông báo mới")
                            .setBody(noiDung)
                            .setIcon("http://qoffice-staging.quangnam.gov.vn/logo-eoffice.svg")
                            .setColor("#f45342")
                            .build();
                    
                    AndroidConfig androidConfig = AndroidConfig.builder()
                            .setTtl(3600 * 1000) // 1 hour in milliseconds
                            .setPriority(AndroidConfig.Priority.NORMAL)
                            .setNotification(androidNotification)
                            .build();
                    
                    mess = Message.builder().putAllData(data)
                            .setToken(device.getToken())
                            .setAndroidConfig(androidConfig)
                            .build();
                } else if (LoaiThietBiEnum.IOS.name().equalsIgnoreCase(device.getLoaiThietBi())) {
                    // Push noti cho iOS
                    System.out.println("gui noti cho ios");
                    ApsAlert apsAlert = ApsAlert.builder()
                            .setTitle("Bạn có một thông báo mới")
                            .setBody(noiDung)
                            .build();
                    
                    Aps aps = Aps.builder()
                            .setBadge(42)
                            .setAlert(apsAlert)
                            .build();
                    
                    ApnsConfig apnsConfig = ApnsConfig.builder()
                            .putHeader("apns-priority", "10")
                            .setAps(aps)
                            .build();
                    mess = Message.builder().putAllData(data)
                            .setToken(device.getToken())
                            .setApnsConfig(apnsConfig)
                            .build();
                }                
                if (mess != null) {
                    try {
                        // Get response from FCM Service
                        String response = FirebaseMessaging.getInstance().sendAsync(mess).get();
                        System.out.println("Sent message: " + response);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IllegalArgumentException e) {
               System.out.println("==========token khong dung: " + device.getToken());
            }
        }   
	}
	
	@Override
    public void sendNotificationWithParamToClient(NotificationWithParamRequest request) {
        try {
            GetDetailThongTinUrlByKeyQuery thongTinUrlQuery = new GetDetailThongTinUrlByKeyQuery();
            thongTinUrlQuery.setKey(request.getKey());
            ThongTinUrlDto thongTinUrlDto = thongTinUrlQueryHandler.handle(thongTinUrlQuery);
            
            if (thongTinUrlDto != null) {
                Object[] param = null;
                if (request.getParams() != null && request.getParams().length > 0) {
                    param = new Object[request.getParams().length];
                    for (int i = 0; i < request.getParams().length; i++) {
                        param[i] = request.getParams()[i].toString();
                    }
                }
                String url = "";
                if (thongTinUrlDto.getUrl() != null) {
                    url = MessageFormat.format(thongTinUrlDto.getUrl(), param);
                }
                String noiDung = "";
                if (request.getNoiDung() != null) {
                	noiDung = request.getNoiDung().length() > 255 ? request.getNoiDung().substring(0, 251) + "..." : request.getNoiDung();
                }
                // Add a new record to Notification table
                CreateNotificationCmd nCmd = new CreateNotificationCmd();
                nCmd.setNoiDung(noiDung);
                nCmd.setType(thongTinUrlDto.getType());
                nCmd.setMobile(thongTinUrlDto.isMobile());
                nCmd.setWeb(thongTinUrlDto.isWeb());
                nCmd.setKey(thongTinUrlDto.getKey());
                Long mainId = null;
                Long secondId = null;
                try {
                    mainId = param != null ? Long.valueOf(param[0].toString()) : null;
                } catch (NumberFormatException e) {}
                if (param != null) {
                    try {
                        mainId =  Long.valueOf(param[0].toString());
                    } catch (NumberFormatException e) {}
                    if (param.length > 1) {
                        for (int i = 1; i < param.length; i++) {
                            try {
                                secondId = Long.valueOf(param[i].toString());
                                break;
                            } catch (NumberFormatException e) {}
                        }
                    }
                }
                nCmd.setMainId(mainId);
                nCmd.setSecondId(secondId);
                nCmd.setUrl(url);

                notificationCmdHandler.handle(nCmd);

                // Convert json string to list of long variable
                List<Long> list = request.getNguoiNhanIds().stream().distinct().collect(Collectors.toList());

                // Add records to NotificationNhanVien table
                for (int i = 0; i < list.size(); i++) {
                    CreateNotificationNhanVienCmd nnCmd = new CreateNotificationNhanVienCmd();
                    nnCmd.setNotificationId(nCmd.getId());
                    nnCmd.setChucDanhId(list.get(i));
                    nnCmd.setDaDoc(0);

                    notificationNhanVienCmdHandler.handle(nnCmd);
                }

                // Get list of device of client
                List<DeviceDTO> devicesWeb = new ArrayList<>();
                List<DeviceDTO> devicesAndroid = new ArrayList<>();
                List<DeviceDTO> devicesIos = new ArrayList<>();
                for (int i = 0; i < list.size(); i++) {
                    GetDsDeviceQuery query = new GetDsDeviceQuery();
                    query.setChucDanhId(list.get(i));
                    System.out.println("chucDanh: " + list.get(i));
                    List<DeviceDTO> iDevices = deviceQueryHandler.handle(query);
                    if (iDevices != null) {
                        for (DeviceDTO d : iDevices) {
                            if (d.getLoaiThietBi() != null) {
                                if (LoaiThietBiEnum.WEB.name().equalsIgnoreCase(d.getLoaiThietBi())) {
                                    devicesWeb.add(d);
                                } else if (LoaiThietBiEnum.ANDROID.name().equalsIgnoreCase(d.getLoaiThietBi())) {
                                    devicesAndroid.add(d);
                                } else if (LoaiThietBiEnum.IOS.name().equalsIgnoreCase(d.getLoaiThietBi())) {
                                    devicesIos.add(d);
                                }
                            }
                        }
                    }
                }

                // Convert object to json object for adding on a request FCM
                Map<String, String> data = new HashMap<>();
                data.put("noiDung", request.getNoiDung());
                data.put("url", url);
                data.put("type", thongTinUrlDto.getType());
                data.put("key", thongTinUrlDto.getKey());
                data.put("mainId", mainId != null ? mainId.toString() : "");
                data.put("secondId", secondId != null ? secondId.toString() : "");
                data.put("id", nCmd.getId().toString());
                
                List<DeviceDTO> devices = new ArrayList<>();
                if (thongTinUrlDto.isWeb()) {
                    devices.addAll(devicesWeb);
                } 
                if (thongTinUrlDto.isMobile()) {
                    devices.addAll(devicesAndroid);
                    devices.addAll(devicesIos);
                }
                
                for (int i = 0; i < devices.size(); i++) {
                    guiNotiDenDevice(devices.get(i), url, request.getNoiDung(), data); 
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.toString());
        }
    }

	@Override
	public void updateDaDoc(Long notificationId, Long chucDanhId) {
		try {
			UpdateDaDocNotificationNhanVienCmd cmd = new UpdateDaDocNotificationNhanVienCmd();
			cmd.setNotificationId(notificationId);
			cmd.setChucDanhId(chucDanhId);

			notificationNhanVienCmdHandler.handle(cmd);
		}
		catch (Exception ex) {
			throw ex;
		}
	}


	@Override
	public void registerDevice(RegisterDeviceRequest request) {
		try {
			CreateDeviceCmd cmd = new CreateDeviceCmd();
			cmd.setLoaiThietBi(request.getLoaiThietBi());
			cmd.setThongTin(request.getThongTin());
			cmd.setToken(request.getToken());
			cmd.setChucDanhId(request.getChucDanhId());

			deviceCmdHandler.handle(cmd);
		} catch (Exception ex) {
			throw ex;
		}
	}

	@Override
	public void updateDevice(UpdateDeviceRequest request) {
		try {
			UpdateDeviceCmd cmd = new UpdateDeviceCmd();
			cmd.setChucDanhId(request.getChucDanhId());
			cmd.setLoaiThietBi(request.getLoaiThietBi());
			cmd.setThongTin(request.getThongTin());
			cmd.setToken(request.getToken());

			deviceCmdHandler.handle(cmd);
		} catch (Exception ex) {
			throw ex;
		}
	}

	@Override
	public void unregisterDevice(UnregisterDeviceRequest request) {
		try {
			DeleteDeviceCmd cmd = new DeleteDeviceCmd();
			cmd.setChucDanhId(request.getChucDanhId());
			cmd.setToken(request.getToken());

			deviceCmdHandler.handle(cmd);
		} catch (Exception ex) {
			throw ex;
		}
	}

	@Override
	public Page<NotificationDTO> getNotificationsByChucDanhId(long chucDanhId, NotificationsStatusEnum status, String loaiThietBi, int page, int limit) {
		try {
			GetDsNotificationQuery query = new GetDsNotificationQuery();
			query.setChucDanhId(chucDanhId);
			query.setStatus(status);
			query.setLoaiThietBi(loaiThietBi);
			query.setPage(page);
			query.setLimit(limit);
			Page<NotificationDTO> result = notificationQueryHandler.handle(query);
			return result;
		} catch (Exception ex) {
			throw ex;
		}
	}

    @Override
    public void createThongTinUrl(CreateThongTinUrlRequest request) {
        try {
            CreateThongTinUrlCmd cmd = new CreateThongTinUrlCmd();
            cmd.setKey(request.getKey());
            cmd.setNoiDung(request.getNoiDung());
            cmd.setUrl(request.getUrl());
            cmd.setMobile(request.isMobile());
            cmd.setWeb(request.isWeb());
            cmd.setType(request.getType());

            thongTinUrlCmdHandler.handle(cmd);
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    @Override
    public void updateThongTinUrl(UpdateThongTinUrlRequest request) {
        try {
            UpdateThongTinUrlCmd cmd = new UpdateThongTinUrlCmd();
            cmd.setKey(request.getKey());
            cmd.setNoiDung(request.getNoiDung());
            cmd.setUrl(request.getUrl());
            cmd.setMobile(request.isMobile());
            cmd.setWeb(request.isWeb());
            cmd.setType(request.getType());

            thongTinUrlCmdHandler.handle(cmd);
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    @Override
    public void deleteThongTinUrl(String key) {
        try {
            DeleteThongTinUrlCmd cmd = new DeleteThongTinUrlCmd();
            cmd.setKey(key);            
            thongTinUrlCmdHandler.handle(cmd);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public List<ThongTinUrlDto> getListThongTinUrl(int page, int limit) {
        try {
            GetDsThongTinUrlQuery query = new GetDsThongTinUrlQuery();
            query.setPage(page);
            query.setLimit(limit);
            List<ThongTinUrlDto> result = new ArrayList<ThongTinUrlDto>();
            result = thongTinUrlQueryHandler.handle(query);
            return result;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public ThongTinUrlDto getDetailThongTinUrl(String key) {
        try {
            GetDetailThongTinUrlByKeyQuery query = new GetDetailThongTinUrlByKeyQuery();
            query.setKey(key);            
            return thongTinUrlQueryHandler.handle(query);
        } catch (Exception ex) {
            throw ex;
        }
    }
}
