package vn.greenglobal.service.query;

import java.util.List;

import vn.greenglobal.domain.Device;
import vn.greenglobal.dto.DeviceDTO;
import vn.greenglobal.service.query.dto.device.GetDetailDeviceByTokenQuery;
import vn.greenglobal.service.query.dto.device.GetDsDeviceQuery;

public interface DeviceQueryHandler {
    List<DeviceDTO> handle(GetDsDeviceQuery query);
    Device handle(GetDetailDeviceByTokenQuery query);
}