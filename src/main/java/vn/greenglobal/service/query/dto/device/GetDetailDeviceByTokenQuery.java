package vn.greenglobal.service.query.dto.device;

public class GetDetailDeviceByTokenQuery {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}