package vn.greenglobal.service.query.dto.notification;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import vn.greenglobal.enums.NotificationsStatusEnum;

@Builder(toBuilder = true)
@ToString(callSuper = true)
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
public class GetDsNotificationQuery {
    private Long chucDanhId;

    private NotificationsStatusEnum status;
    
    private String loaiThietBi;

    private Integer page;

    private Integer limit;
    
}