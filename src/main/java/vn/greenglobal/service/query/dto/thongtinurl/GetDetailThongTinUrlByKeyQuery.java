package vn.greenglobal.service.query.dto.thongtinurl;

public class GetDetailThongTinUrlByKeyQuery {
    
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}