package vn.greenglobal.service.query.dto.device;

public class GetDsDeviceQuery {
    private Long chucDanhId;

    public Long getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }
}