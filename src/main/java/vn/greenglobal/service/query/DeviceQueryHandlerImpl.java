package vn.greenglobal.service.query;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.greenglobal.domain.Device;
import vn.greenglobal.dto.DeviceDTO;
import vn.greenglobal.repository.DeviceRepository;
import vn.greenglobal.service.query.dto.device.GetDetailDeviceByTokenQuery;
import vn.greenglobal.service.query.dto.device.GetDsDeviceQuery;

@Component
public class DeviceQueryHandlerImpl implements DeviceQueryHandler {

    @Autowired
    private DeviceRepository repo;

    @Override
    public List<DeviceDTO> handle(GetDsDeviceQuery query) {
        try {
            List<DeviceDTO> result = repo.findDeviceByChucDanhId(query.getChucDanhId());
            
            
            if (result != null && result.size() > 0) {
                return result;
            }
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public Device handle(GetDetailDeviceByTokenQuery query) {
        // TODO Auto-generated method stub
        Optional<Device> optional = repo.findByTokenAndDaXoaFalse(query.getToken());
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

}