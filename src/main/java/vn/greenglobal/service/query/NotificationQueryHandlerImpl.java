package vn.greenglobal.service.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vn.greenglobal.dto.NotificationDTO;
import vn.greenglobal.enums.LoaiThietBiEnum;
import vn.greenglobal.enums.NotificationsStatusEnum;
import vn.greenglobal.repository.NotificationRepository;
import vn.greenglobal.service.query.dto.notification.GetDsNotificationQuery;

@Component
public class NotificationQueryHandlerImpl implements NotificationQueryHandler {

    @Autowired
    private NotificationRepository repo;

    @Override
    public Page<NotificationDTO> handle(GetDsNotificationQuery query) {
        try {
            Pageable pageable = PageRequest.of(query.getPage(), query.getLimit());
            Page<NotificationDTO> result;
            if (query.getLoaiThietBi() != null && LoaiThietBiEnum.WEB.name().equalsIgnoreCase(query.getLoaiThietBi())) {
                if (query.getStatus() == null || NotificationsStatusEnum.ALL.equals(query.getStatus())) {
                    result = repo.findNotificationsByChucDanhIdWeb(query.getChucDanhId(), pageable);
                } else {
                    boolean daDoc = query.getStatus().equals(NotificationsStatusEnum.READ);
                    result = repo.findNotificationsByChucDanhIdAndDaDocWeb(query.getChucDanhId(), daDoc, pageable);
                }
            } else if (query.getLoaiThietBi() != null && (LoaiThietBiEnum.ANDROID.name().equalsIgnoreCase(query.getLoaiThietBi()) 
                    || LoaiThietBiEnum.IOS.name().equalsIgnoreCase(query.getLoaiThietBi()))) {
                if (query.getStatus() == null || NotificationsStatusEnum.ALL.equals(query.getStatus())) {
                    result = repo.findNotificationsByChucDanhIdMobile(query.getChucDanhId(), pageable);
                } else {
                    boolean daDoc = query.getStatus().equals(NotificationsStatusEnum.READ);
                    result = repo.findNotificationsByChucDanhIdAndDaDocMobile(query.getChucDanhId(), daDoc, pageable);
                }
            } else {
                if (query.getStatus() == null || NotificationsStatusEnum.ALL.equals(query.getStatus())) {
                    result = repo.findNotificationsByChucDanhId(query.getChucDanhId(), pageable);
                } else {
                    boolean daDoc = query.getStatus().equals(NotificationsStatusEnum.READ);
                    result = repo.findNotificationsByChucDanhIdAndDaDoc(query.getChucDanhId(), daDoc, pageable);
                }
            }
            
            return result;
        } catch (Exception ex) {
            throw ex;
        }
    }
}