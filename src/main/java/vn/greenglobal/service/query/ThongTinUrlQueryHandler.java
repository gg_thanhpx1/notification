package vn.greenglobal.service.query;

import java.util.List;

import vn.greenglobal.dto.ThongTinUrlDto;
import vn.greenglobal.service.query.dto.thongtinurl.GetDetailThongTinUrlByKeyQuery;
import vn.greenglobal.service.query.dto.thongtinurl.GetDsThongTinUrlQuery;

public interface ThongTinUrlQueryHandler {
    List<ThongTinUrlDto> handle(GetDsThongTinUrlQuery query);
    ThongTinUrlDto handle(GetDetailThongTinUrlByKeyQuery query);
}