package vn.greenglobal.service.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vn.greenglobal.dto.ThongTinUrlDto;
import vn.greenglobal.repository.ThongTinUrlRepository;
import vn.greenglobal.service.query.dto.thongtinurl.GetDetailThongTinUrlByKeyQuery;
import vn.greenglobal.service.query.dto.thongtinurl.GetDsThongTinUrlQuery;

@Component
public class ThongTinUrlQueryHandlerImpl implements ThongTinUrlQueryHandler {

    @Autowired
    private ThongTinUrlRepository repo;

    @Override
    public List<ThongTinUrlDto> handle(GetDsThongTinUrlQuery query) {
        try {
            Pageable pageable = PageRequest.of(query.getPage(), query.getLimit());
            List<ThongTinUrlDto> result;
            result = repo.listThongTinUrl(pageable);
            if (result == null) {
                result = new ArrayList<ThongTinUrlDto>();
            }
            return result;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public ThongTinUrlDto handle(GetDetailThongTinUrlByKeyQuery query) {
        // TODO Auto-generated method stub
        Optional<ThongTinUrlDto> optional = repo.findByKey(query.getKey());
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

}