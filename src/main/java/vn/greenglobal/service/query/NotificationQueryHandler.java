package vn.greenglobal.service.query;

import java.util.List;

import org.springframework.data.domain.Page;

import vn.greenglobal.dto.NotificationDTO;
import vn.greenglobal.service.query.dto.notification.GetDsNotificationQuery;

public interface NotificationQueryHandler {
    Page<NotificationDTO> handle(GetDsNotificationQuery query);
}