package vn.greenglobal.service.command;

import vn.greenglobal.service.command.dto.notification.CreateNotificationCmd;

public interface NotificationCmdHandler {
    void handle(CreateNotificationCmd cmd);
}