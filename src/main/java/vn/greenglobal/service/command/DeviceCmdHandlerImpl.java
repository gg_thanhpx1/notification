package vn.greenglobal.service.command;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.greenglobal.domain.Device;
import vn.greenglobal.repository.DeviceRepository;
import vn.greenglobal.service.command.dto.device.CreateDeviceCmd;
import vn.greenglobal.service.command.dto.device.DeleteDeviceCmd;
import vn.greenglobal.service.command.dto.device.UpdateDeviceCmd;

@Component
public class DeviceCmdHandlerImpl implements DeviceCmdHandler {

    @Autowired
    private DeviceRepository repo;

    private Logger logger = LoggerFactory.getLogger(DeviceCmdHandlerImpl.class);

    @Override
    public void handle(CreateDeviceCmd cmd) {
        try {
            
            Optional<Device> optional = repo.findByTokenAndDaXoaFalse(cmd.getToken());
            Device entity = null;
            if (optional.isPresent()) {
                entity = optional.get();
                entity.setChucDanhId(cmd.getChucDanhId());
                entity.setThongTin(cmd.getThongTin());
                entity.setLoaiThietBi(cmd.getLoaiThietBi());
            } else {
                entity = new Device();
                entity.setChucDanhId(cmd.getChucDanhId());
                entity.setToken(cmd.getToken());
                entity.setThongTin(cmd.getThongTin());
                entity.setLoaiThietBi(cmd.getLoaiThietBi());
            }
            repo.save(entity);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void handle(UpdateDeviceCmd cmd) {
        try {
            Optional<Device> optional = repo.findByChucDanhIdAndLoaiThietBiAndThongTin(cmd.getChucDanhId(), cmd.getLoaiThietBi(), cmd.getThongTin());

            if (optional.isPresent()) {
                Device entity = optional.get();
                entity.setToken(cmd.getToken());
                repo.save(entity);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void handle(DeleteDeviceCmd cmd) {
        try {
            repo.deleteByChucDanhIdAndToken(cmd.getChucDanhId(), cmd.getToken());
        } catch (Exception ex) {
            logger.error("Error : " + ex.toString());
            
            throw ex;
        }
    }
}