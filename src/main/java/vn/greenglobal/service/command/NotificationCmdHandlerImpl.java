package vn.greenglobal.service.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.greenglobal.domain.Notification;
import vn.greenglobal.repository.NotificationRepository;
import vn.greenglobal.service.command.dto.notification.CreateNotificationCmd;

@Component
public class NotificationCmdHandlerImpl implements NotificationCmdHandler {

    @Autowired
    private NotificationRepository repo;

    @Override
    public void handle(CreateNotificationCmd cmd) {
        try {
            Notification entity = new Notification();
            entity.setNoiDung(cmd.getNoiDung());
            entity.setUrl(cmd.getUrl());
            entity.setType(cmd.getType());
            entity.setMobile(cmd.isMobile());
            entity.setWeb(cmd.isWeb());
            entity.setMainId(cmd.getMainId());
            entity.setSecondId(cmd.getSecondId());
            entity.setKey(cmd.getKey());
            repo.save(entity);
            
            cmd.setId(entity.getId());
        } catch (Exception ex) {
            throw ex;
        }
    }
}