package vn.greenglobal.service.command;

import vn.greenglobal.service.command.dto.notification_nhanvien.CreateNotificationNhanVienCmd;
import vn.greenglobal.service.command.dto.notification_nhanvien.UpdateDaDocNotificationNhanVienCmd;

public interface NotificationNhanVienCmdHandler {
    void handle(CreateNotificationNhanVienCmd cmd);

    void handle(UpdateDaDocNotificationNhanVienCmd cmd);
}