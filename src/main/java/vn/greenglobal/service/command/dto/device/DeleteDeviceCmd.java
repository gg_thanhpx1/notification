package vn.greenglobal.service.command.dto.device;

public class DeleteDeviceCmd {
    
    private Long chucDanhId;
    private String token;


    public Long getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}