package vn.greenglobal.service.command.dto.thongtinurl;

public class DeleteThongTinUrlCmd {
    
    private String key;
        
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}