package vn.greenglobal.service.command.dto.thongtinurl;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString(callSuper = true)
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
public class CreateThongTinUrlCmd {

    private String key;
    private String url;
    private String noiDung;
    private String type; 
    private boolean web;
    private boolean mobile;
}