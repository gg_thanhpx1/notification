package vn.greenglobal.service.command.dto.notification_nhanvien;

public class UpdateDaDocNotificationNhanVienCmd {
    private Long notificationId;

    private Long chucDanhId;

    /**
     * @return the notificationId
     */
    public Long getNotificationId() {
        return notificationId;
    }

    public Long getChucDanhId() {
        return chucDanhId;
    }



    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }
    
    /**
     * @param notificationId the notificationId to set
     */
    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }
}