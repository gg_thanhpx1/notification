package vn.greenglobal.service.command.dto.device;

public class CreateDeviceCmd {

    private Long chucDanhId;

    private String token;

    private String thongTin;

    private String loaiThietBi;

    /**
     * @return the loaiThietBi
     */
    public String getLoaiThietBi() {
        return loaiThietBi;
    }

    /**
     * @param loaiThietBi the loaiThietBi to set
     */
    public void setLoaiThietBi(String loaiThietBi) {
        this.loaiThietBi = loaiThietBi;
    }

    /**
     * @return the thongTin
     */
    public String getThongTin() {
        return thongTin;
    }

    /**
     * @param thongTin the thongTin to set
     */
    public void setThongTin(String thongTin) {
        this.thongTin = thongTin;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the chucDanhId
     */
    public Long getChucDanhId() {
        return chucDanhId;
    }

    /**
     * @param chucDanhId the chucDanhId to set
     */
    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }
}