package vn.greenglobal.service.command.dto.notification;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
public class CreateNotificationCmd {
	private Long id;
	private Long nguoiTaoId;
	private Long mainId;
	private Long secondId;
	
	private String noiDung;
	private String url;
	private String tenNguoiTao;
    private String type; 
    private String key; 
	
	private boolean web;
	private boolean mobile;
}