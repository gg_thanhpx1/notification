package vn.greenglobal.service.command.dto.notification_nhanvien;

public class CreateNotificationNhanVienCmd {
    private Long notificationId;

    private Long chucDanhId;

    private Integer daDoc;

    /**
     * @return the notificationId
     */
    public Long getNotificationId() {
        return notificationId;
    }

    /**
     * @return the daDoc
     */
    public Integer getDaDoc() {
        return daDoc;
    }

    /**
     * @param daDoc the daDoc to set
     */
    public void setDaDoc(Integer daDoc) {
        this.daDoc = daDoc;
    }

    public Long getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }

    /**
     * @param notificationId the notificationId to set
     */
    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }
}