package vn.greenglobal.service.command;

import vn.greenglobal.service.command.dto.device.CreateDeviceCmd;
import vn.greenglobal.service.command.dto.device.DeleteDeviceCmd;
import vn.greenglobal.service.command.dto.device.UpdateDeviceCmd;

public interface DeviceCmdHandler {
    void handle(CreateDeviceCmd cmd);

    void handle(UpdateDeviceCmd cmd);

    void handle(DeleteDeviceCmd cmd);
}