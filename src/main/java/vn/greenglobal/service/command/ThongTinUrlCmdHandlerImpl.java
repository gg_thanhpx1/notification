package vn.greenglobal.service.command;

import java.time.Instant;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.greenglobal.domain.ThongTinUrl;
import vn.greenglobal.repository.ThongTinUrlRepository;
import vn.greenglobal.service.command.dto.thongtinurl.CreateThongTinUrlCmd;
import vn.greenglobal.service.command.dto.thongtinurl.DeleteThongTinUrlCmd;
import vn.greenglobal.service.command.dto.thongtinurl.UpdateThongTinUrlCmd;
import vn.tcx.framework.config.TcxConstants;
import vn.tcx.framework.security.SecurityUtils;

@Component
public class ThongTinUrlCmdHandlerImpl implements ThongTinUrlCmdHandler {

    @Autowired
    private ThongTinUrlRepository repo;

    @Override
    public void handle(CreateThongTinUrlCmd cmd) {
        try {
            
            Optional<ThongTinUrl> optional = repo.findByKeyAndDaXoaFalse(cmd.getKey());
            ThongTinUrl entity = null;
            if (optional.isPresent()) {
                entity = optional.get();
                entity.setKey(cmd.getKey());
                entity.setNoiDung(cmd.getNoiDung());
                entity.setUrl(cmd.getUrl());
                entity.setMobile(cmd.isMobile());
                entity.setWeb(cmd.isWeb());
                entity.setType(cmd.getType());
            } else {
                entity = new ThongTinUrl();
                entity.setKey(cmd.getKey());
                entity.setNoiDung(cmd.getNoiDung());
                entity.setUrl(cmd.getUrl());
                entity.setMobile(cmd.isMobile());
                entity.setWeb(cmd.isWeb());
                entity.setType(cmd.getType());
            }
            repo.save(entity);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void handle(UpdateThongTinUrlCmd cmd) {
        try {
            Optional<ThongTinUrl> optional = repo.findByKeyAndDaXoaFalse(cmd.getKey());

            if (optional.isPresent()) {
                ThongTinUrl entity = optional.get();
                entity.setKey(cmd.getKey());
                entity.setNoiDung(cmd.getNoiDung());
                entity.setUrl(cmd.getUrl());
                entity.setMobile(cmd.isMobile());
                entity.setWeb(cmd.isWeb());
                entity.setType(cmd.getType());
                repo.save(entity);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }
    
    @Override
    public void handle(DeleteThongTinUrlCmd cmd) {
        try {
            Optional<ThongTinUrl> optional = repo.findByKeyAndDaXoaFalse(cmd.getKey());

            if (optional.isPresent()) {
                ThongTinUrl entity = optional.get();
                entity.setNgayXoa(Instant.now());
                entity.setNguoiXoa(SecurityUtils.getCurrentUserLogin().orElse(TcxConstants.ANONYMOUS_USER));
                entity.setDaXoa(true);
                repo.save(entity);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }
}