package vn.greenglobal.service.command;

import vn.greenglobal.service.command.dto.thongtinurl.CreateThongTinUrlCmd;
import vn.greenglobal.service.command.dto.thongtinurl.DeleteThongTinUrlCmd;
import vn.greenglobal.service.command.dto.thongtinurl.UpdateThongTinUrlCmd;

public interface ThongTinUrlCmdHandler {
    void handle(CreateThongTinUrlCmd cmd);

    void handle(UpdateThongTinUrlCmd cmd);
    
    void handle(DeleteThongTinUrlCmd cmd);
}