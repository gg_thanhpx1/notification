package vn.greenglobal.service.command;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vn.greenglobal.domain.NotificationNhanVien;
import vn.greenglobal.repository.NotificationNhanVienRepository;
import vn.greenglobal.service.command.dto.notification_nhanvien.CreateNotificationNhanVienCmd;
import vn.greenglobal.service.command.dto.notification_nhanvien.UpdateDaDocNotificationNhanVienCmd;

@Component
public class NotificationNhanVienCmdHandlerImpl implements NotificationNhanVienCmdHandler {

    @Autowired
    private NotificationNhanVienRepository repo;

    private Logger logger = LoggerFactory.getLogger(NotificationNhanVienCmdHandlerImpl.class);

    @Override
    public void handle(CreateNotificationNhanVienCmd cmd) {
        try {
            NotificationNhanVien entity = new NotificationNhanVien();
            entity.setNotificationId(cmd.getNotificationId());
            entity.setChucDanhId(cmd.getChucDanhId());
            entity.setDaDoc(false);

            repo.save(entity);
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public void handle(UpdateDaDocNotificationNhanVienCmd cmd) {
        try {
            Optional<NotificationNhanVien> optional = repo.findByNotificationIdAndChucDanhId(cmd.getNotificationId(), cmd.getChucDanhId());

            if (optional.isPresent()) {
                NotificationNhanVien entity = optional.get();
                entity.setDaDoc(true);
                repo.save(entity);
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

}