package vn.greenglobal.service;

import java.util.List;

import org.springframework.data.domain.Page;

import vn.greenglobal.dto.NotificationDTO;
import vn.greenglobal.dto.ThongTinUrlDto;
import vn.greenglobal.enums.NotificationsStatusEnum;
import vn.greenglobal.model.request.CreateThongTinUrlRequest;
import vn.greenglobal.model.request.NotificationRequest;
import vn.greenglobal.model.request.NotificationWithParamRequest;
import vn.greenglobal.model.request.RegisterDeviceRequest;
import vn.greenglobal.model.request.UnregisterDeviceRequest;
import vn.greenglobal.model.request.UpdateDeviceRequest;
import vn.greenglobal.model.request.UpdateThongTinUrlRequest;

public interface PushNotificationService {
	//Notification
	void updateDaDoc(Long notificationId, Long chucDanhId);
	void sendNotificationToClient(NotificationRequest request);
	void sendNotificationWithParamToClient(NotificationWithParamRequest request);
	Page<NotificationDTO> getNotificationsByChucDanhId(long chucDanhId, NotificationsStatusEnum status, String loaiThietBi, int page, int limit);
	
	//Device
	void registerDevice(RegisterDeviceRequest request);
	void updateDevice(UpdateDeviceRequest request);
    void unregisterDevice(UnregisterDeviceRequest request);
    
    //ThongTinUrl
    void createThongTinUrl(CreateThongTinUrlRequest request);
    void updateThongTinUrl(UpdateThongTinUrlRequest request);
    void deleteThongTinUrl(String key);
    List<ThongTinUrlDto> getListThongTinUrl(int page, int limit);
    ThongTinUrlDto getDetailThongTinUrl(String key);
}
