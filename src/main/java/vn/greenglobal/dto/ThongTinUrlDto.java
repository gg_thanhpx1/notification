package vn.greenglobal.dto;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
public class ThongTinUrlDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;	
	private String key;
    private String noiDung;
    private String url;
    private String type; 
    private boolean web;
    private boolean mobile;
    

    public ThongTinUrlDto(Long id, String noiDung, String url, String key, String type, boolean web, boolean mobile) {
    	this.id = id;
    	this.noiDung = noiDung;
    	this.url = url;
    	this.key = key;
    	this.type = type;
    	this.web = web;
    	this.mobile = mobile;
    }


}
