package vn.greenglobal.dto;

import java.io.Serializable;
import java.time.Instant;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString(callSuper = true)
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
public class NotificationDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
    private String noiDung;
    private String url;    
    private boolean daDoc;    
    private Instant ngayTao;
    private String type; 
    private Long mainId;
    private String key; 
    private Long secondId;
    
    public NotificationDTO(Long id, String noiDung, String url, boolean daDoc, Instant ngayTao, String type,
            Long mainId, String key, Long secondId) {
        this.id = id;
        this.noiDung = noiDung;
        this.url = url;
        this.daDoc = daDoc;
        this.ngayTao = ngayTao;
        this.type = type;
        this.mainId = mainId;
        this.key = key;
        this.secondId = secondId;
    }
}
