package vn.greenglobal.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString(callSuper = true)
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
public class DeviceDTO {
	private Long id;
	
	private String token;
	
	private Long chucDanhId;
	
	private String loaiThietBi;

	public DeviceDTO(Long id, String token, Long chucDanhId, String loaiThietBi) {
		this.id = id;
		this.token = token;
		this.chucDanhId = chucDanhId;
		this.loaiThietBi = loaiThietBi;
	}
}
