package vn.greenglobal.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Utils {
	public static ResponseEntity<Object> ResponseResult(Object data) {
		Map<String, Object> result = new HashMap<>();
		result.put("data", data);
		
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}
	
	public static ResponseEntity<Object> ResponseResult(HttpStatus httpStatus, String message) {
		Map<String, Object> error = new HashMap<>();
		error.put("status", httpStatus.value());
		error.put("message", message);
		
		return ResponseEntity.status(httpStatus.value()).body(error);
	}
}
