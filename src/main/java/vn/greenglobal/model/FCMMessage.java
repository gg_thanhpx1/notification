package vn.greenglobal.model;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FCMMessage {

	@JsonProperty
	@NotEmpty
	private String to;
	
	@JsonProperty
	private String[] registration_ids;
	
	@JsonProperty
	private String condition;
	
	@JsonProperty
	private String collapse_key;
	
	@JsonProperty
	private String priority;
	
	@JsonProperty
	private boolean content_available;
	
	@JsonProperty
	private boolean mutable_content;
	
	@JsonProperty
	private int time_to_live;
	
	@JsonProperty
	private String restricted_package_name;
	
	@JsonProperty
	private boolean dry_run;
	
	@JsonProperty
	@NotEmpty
	private FCMData notification;

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String[] getRegistration_ids() {
		return registration_ids;
	}

	public void setRegistration_ids(String[] registration_ids) {
		this.registration_ids = registration_ids;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getCollapse_key() {
		return collapse_key;
	}

	public void setCollapse_key(String collapse_key) {
		this.collapse_key = collapse_key;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public boolean isContent_available() {
		return content_available;
	}

	public void setContent_available(boolean content_available) {
		this.content_available = content_available;
	}

	public boolean isMutable_content() {
		return mutable_content;
	}

	public void setMutable_content(boolean mutable_content) {
		this.mutable_content = mutable_content;
	}

	public int getTime_to_live() {
		return time_to_live;
	}

	public void setTime_to_live(int time_to_live) {
		this.time_to_live = time_to_live;
	}

	public String getRestricted_package_name() {
		return restricted_package_name;
	}

	public void setRestricted_package_name(String restricted_package_name) {
		this.restricted_package_name = restricted_package_name;
	}

	public boolean isDry_run() {
		return dry_run;
	}

	public void setDry_run(boolean dry_run) {
		this.dry_run = dry_run;
	}

	public FCMData getNotification() {
		return notification;
	}

	public void setNotification(FCMData notification) {
		this.notification = notification;
	}
	
}
