package vn.greenglobal.model.request;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
public class UpdateThongTinUrlRequest {

    private String key;
    private String url;
    private String noiDung;
    private String type; 
    private boolean web;
    private boolean mobile;
}