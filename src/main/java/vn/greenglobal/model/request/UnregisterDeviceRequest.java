package vn.greenglobal.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class UnregisterDeviceRequest {
    
    @JsonIgnore
    private Long chucDanhId;
    private String token;

    public Long getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}