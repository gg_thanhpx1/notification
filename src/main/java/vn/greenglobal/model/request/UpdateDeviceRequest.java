package vn.greenglobal.model.request;

public class UpdateDeviceRequest {
    private long chucDanhId;

    private String loaiThietBi;

    private String thongTin;

    private String token;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the thongTin
     */
    public String getThongTin() {
        return thongTin;
    }

    /**
     * @param thongTin the thongTin to set
     */
    public void setThongTin(String thongTin) {
        this.thongTin = thongTin;
    }

    /**
     * @return the loaiThietBi
     */
    public String getLoaiThietBi() {
        return loaiThietBi;
    }

    /**
     * @param loaiThietBi the loaiThietBi to set
     */
    public void setLoaiThietBi(String loaiThietBi) {
        this.loaiThietBi = loaiThietBi;
    }

    public long getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }
}