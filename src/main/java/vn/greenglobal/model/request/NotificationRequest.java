package vn.greenglobal.model.request;

import java.util.List;

public class NotificationRequest {
	private String noiDung;

	private String url;

	private List<Long> nguoiNhanIds;

	/**
	 * @return the noiDung
	 */
	public String getNoiDung() {
		return noiDung;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * @param noiDung the noiDung to set
	 */
	public void setNoiDung(String noiDung) {
		this.noiDung = noiDung;
	}

    public List<Long> getNguoiNhanIds() {
        return nguoiNhanIds;
    }

    public void setNguoiNhanIds(List<Long> nguoiNhanIds) {
        this.nguoiNhanIds = nguoiNhanIds;
    }
}
