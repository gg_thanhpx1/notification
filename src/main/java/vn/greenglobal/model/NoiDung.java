package vn.greenglobal.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NoiDung {
	@JsonProperty
	private String hanhDong;
	
	@JsonProperty
	private String chucNang;
	
	@JsonProperty
	private String tenNguoiGui;
}
