package vn.greenglobal.helper;

import org.modelmapper.ModelMapper;

import vn.greenglobal.domain.Notification;
import vn.greenglobal.dto.NotificationDTO;

public class Mapper {
    private static ModelMapper mapper = new ModelMapper();

    public static NotificationDTO convertToDto(Notification notification) {
        NotificationDTO notificationDto = mapper.map(notification, NotificationDTO.class);
        return notificationDto;
    }  
}