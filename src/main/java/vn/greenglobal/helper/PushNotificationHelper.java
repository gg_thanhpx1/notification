package vn.greenglobal.helper;

import org.json.JSONObject;

public class PushNotificationHelper {
	public static JSONObject convertToJsonObject(String message, String nhanVienToken) {
		JSONObject body = new JSONObject();
		body.put("to", nhanVienToken);
		
		JSONObject notification = new JSONObject();
		notification.put("title", "Testing Device");
		notification.put("body", message);
		notification.put("click_action", "");
		notification.put("icon", "");
		
		body.put("notification", notification);
		
		return body;
	}
}
