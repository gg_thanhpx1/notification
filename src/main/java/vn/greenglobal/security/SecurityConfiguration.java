package vn.greenglobal.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import(SecurityProblemSupport.class)
public class SecurityConfiguration extends ResourceServerConfigurerAdapter {

	@Value("${security.byPassApiSecure:true}")
	private Boolean byPassApiSecure;
	
	@Value("${security.oauth2.spGroup:ROLE_ADMIN}")
	private String spGroup;
	
	@Autowired
    private CustomBasicAuthenticationEntryPoint authenticationEntryPoint;
	
    private final SecurityProblemSupport problemSupport;

    public SecurityConfiguration(SecurityProblemSupport problemSupport) {
        this.problemSupport = problemSupport;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
            .disable()
            .exceptionHandling()
            .authenticationEntryPoint(authenticationEntryPoint)
            .accessDeniedHandler(problemSupport)
        .and()
            .headers()
            .frameOptions()
            .disable()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        if(byPassApiSecure) {
            http.authorizeRequests()
	             .antMatchers("/api/**").permitAll()
	             .antMatchers("/api/truc-lien-thong/dong-bo-van-ban").permitAll()
	             .antMatchers("/management/health").permitAll()
	             .antMatchers("/management/info").permitAll()
	             .antMatchers("/management/**").hasAuthority("ROLE_ADMIN");
        } else {
        	 http.authorizeRequests()
        	 	 .antMatchers(HttpMethod.OPTIONS, "/api/**").permitAll()
	             .antMatchers("/api/**").hasRole(spGroup)
	             .antMatchers("/management/health").permitAll()
	             .antMatchers("/management/info").permitAll()
	             .antMatchers("/management/**").hasAuthority("ROLE_ADMIN")
	             .antMatchers("/**").permitAll();
        }
    }

    /**
     * This OAuth2RestTemplate is only used by AuthorizationHeaderUtil that is currently used by TokenRelayRequestInterceptor
     */
    @Bean
    public OAuth2RestTemplate oAuth2RestTemplate(OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails,
        OAuth2ClientContext oAuth2ClientContext) {
        return new OAuth2RestTemplate(oAuth2ProtectedResourceDetails, oAuth2ClientContext);
    }

    @Primary
    @Bean
    public OAuth2ClientContext singletonClientContext() {
        return new DefaultOAuth2ClientContext();
    }
}
