package vn.greenglobal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import vn.greenglobal.enums.ApiMessageEnum;
import vn.greenglobal.model.request.RegisterDeviceRequest;
import vn.greenglobal.model.request.UnregisterDeviceRequest;
import vn.greenglobal.model.request.UpdateDeviceRequest;
import vn.greenglobal.service.PushNotificationService;
import vn.greenglobal.util.Utils;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api/devices")
public class DeviceController {
	@Autowired
	private PushNotificationService pushNotificationsService;
		
	@PostMapping
	@ApiOperation(value="Đăng kí device", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> registerDevice(
	        @RequestHeader(name = "chuc-danh-id", defaultValue = "-1", required = true) Long chucDanhId, 
	        @RequestBody RegisterDeviceRequest request) {
		try {
			this.pushNotificationsService.registerDevice(request);

			return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
		} catch (Exception ex) {
		    ex.printStackTrace();
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
		}
	}

	@PutMapping
	@ApiOperation(value="Cập nhập token cho thiết bị", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> updateDevice(
	        @RequestHeader(name = "chuc-danh-id", defaultValue = "-1", required = true) Long chucDanhId,
	        @RequestBody UpdateDeviceRequest request) {
		try {
			this.pushNotificationsService.updateDevice(request);

			return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
		} catch (Exception ex) {
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
		}
	}

	@DeleteMapping
	@ApiOperation(value="Hủy đăng kí device", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> unregisterDevice(
	        @RequestHeader(name = "chuc-danh-id", defaultValue = "-1", required = true) Long chucDanhId,
	        @RequestBody UnregisterDeviceRequest request) {
		try {
		    request.setChucDanhId(chucDanhId);
			pushNotificationsService.unregisterDevice(request);

			return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
		} catch (Exception ex) {
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
		}
	}
}
