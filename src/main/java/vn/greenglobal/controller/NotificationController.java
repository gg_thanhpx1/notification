package vn.greenglobal.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import vn.greenglobal.dto.NotificationDTO;
import vn.greenglobal.enums.ApiMessageEnum;
import vn.greenglobal.enums.NotificationsStatusEnum;
import vn.greenglobal.model.request.NotificationRequest;
import vn.greenglobal.model.request.NotificationWithParamRequest;
import vn.greenglobal.service.PushNotificationService;
import vn.greenglobal.util.Utils;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api/notifications")
public class NotificationController {
	@Autowired
	private PushNotificationService pushNotificationsService;
	
	private Logger logger = LoggerFactory.getLogger(NotificationController.class);
	
	@PostMapping
	@ApiOperation(value="Gửi notification đến người nhận", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> send(
				@RequestBody NotificationRequest request) {
		try {
			// Send notification base on topic			
			// pushNotificationsService.sendNotificationToTopic(request);
			
			// Send notification base on token
			pushNotificationsService.sendNotificationToClient(request);

			return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
		} catch (Exception ex) {
		    ex.printStackTrace();
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.toString());
		}
	}
	
	@PostMapping("/with-params")
    @ApiOperation(value="Gửi notification đến người nhận", produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Object> sendWithParams(
                @RequestBody NotificationWithParamRequest request) {
        try {
            System.out.println("====request: " + request.getParams());
            pushNotificationsService.sendNotificationWithParamToClient(request);

            return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.toString());
        }
    }
	
	@GetMapping
	@ApiOperation(value="Lấy danh sách notification", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> getNotifications(
	            @RequestHeader(name = "chuc-danh-id", defaultValue = "-1", required = true) Long chucDanhId,
				@RequestParam(value = "status", required = false) NotificationsStatusEnum status,
				@RequestParam(value = "loai_thiet_bi", required = false) String loaiThietBi,
				@RequestParam(value = "page", required = true, defaultValue = "1") int page,
				@RequestParam(value = "limit", required = true, defaultValue = "15") int limit) {
		try {
		    page = page - 1;
		    page = page < 0 ? 0 : page;
		    Page<NotificationDTO> result = pushNotificationsService.getNotificationsByChucDanhId(chucDanhId, status, loaiThietBi, page, limit);
			
			return Utils.ResponseResult(result);
			
		} catch (Exception ex) {
			logger.error(ex.toString());
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
		}
	}
	
	@PutMapping("/{id}")
	@ApiOperation(value="Cập nhập trang thái đã đọc của notification", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> updateDaDoc(
				@PathVariable("id") long notificationId, 
				@RequestHeader(name = "chuc-danh-id", defaultValue = "-1", required = true) Long chucDanhId) {
		try {
			this.pushNotificationsService.updateDaDoc(notificationId, chucDanhId);

			return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
		} catch (Exception ex) {
		    ex.printStackTrace();
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
		}
	}
	
	
	// @PostMapping("/subscribe")
	// @ApiOperation(value="Đăng kí nhận notification", position=3, produces=MediaType.APPLICATION_JSON_VALUE)
	// public @ResponseBody ResponseEntity<Object> subscribe(@RequestBody TokenDTO token) {
	// 	try {
	// 		this.pushNotificationsService.subscribe("chuck", token.getToken());
		    
	// 		return Utils.ResponseNoContent();
	// 	}
	// 	catch (Exception ex) {
	// 		logger.info(ex.toString());
	// 		return Utils.ResponseError(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
	// 	}
	// }
}
