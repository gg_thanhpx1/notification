package vn.greenglobal.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import vn.greenglobal.dto.ThongTinUrlDto;
import vn.greenglobal.enums.ApiMessageEnum;
import vn.greenglobal.model.request.CreateThongTinUrlRequest;
import vn.greenglobal.model.request.UpdateThongTinUrlRequest;
import vn.greenglobal.service.PushNotificationService;
import vn.greenglobal.util.Utils;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api/thong-tin-url")
public class ThongTinUrlController {
	@Autowired
	private PushNotificationService pushNotificationsService;
		
	@GetMapping
    @ApiOperation(value="Lấy danh sách Thông tin URL", produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Object> getListThongTinUrl(
                @RequestParam(value = "page", required = true, defaultValue = "1") int page,
                @RequestParam(value = "limit", required = true, defaultValue = "10") int limit) {
        try {
            List<ThongTinUrlDto> result = new ArrayList<ThongTinUrlDto>();
            page = page - 1;
            page = page < 0 ? 0 : page;
            result = pushNotificationsService.getListThongTinUrl(page, limit);
            
            return Utils.ResponseResult(result);
            
        } catch (Exception ex) {
            return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
        }
    }
	
	@GetMapping("/{key}")
    @ApiOperation(value="Lấy chi tiết Thông tin URL", produces=MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<Object> getDetailThongTinUrl(
            @PathVariable(name = "key", required = true) String key) {
        try {
            ThongTinUrlDto result = pushNotificationsService.getDetailThongTinUrl(key);
            
            return Utils.ResponseResult(result);
            
        } catch (Exception ex) {
            return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
        }
    }
	
	@PostMapping
	@ApiOperation(value="Tạo mới Thông tin URL", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> registerDevice(
	        @RequestBody CreateThongTinUrlRequest request) {
		try {
			this.pushNotificationsService.createThongTinUrl(request);

			return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
		} catch (Exception ex) {
		    ex.printStackTrace();
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
		}
	}

	@PutMapping
	@ApiOperation(value="Cập nhập Thông tin URL", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> updateDevice(
	        @RequestBody UpdateThongTinUrlRequest request) {
		try {
			this.pushNotificationsService.updateThongTinUrl(request);

			return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
		} catch (Exception ex) {
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
		}
	}

	@DeleteMapping("/{key}")
	@ApiOperation(value="Xóa thông tin URL", produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> unregisterDevice(
	        @PathVariable(name = "key", required = true) String key) {
		try {
			pushNotificationsService.deleteThongTinUrl(key);

			return Utils.ResponseResult(HttpStatus.OK, ApiMessageEnum.OK.toString());
		} catch (Exception ex) {
			return Utils.ResponseResult(HttpStatus.INTERNAL_SERVER_ERROR, ApiMessageEnum.INTERNAL_SERVER_ERROR.getText());
		}
	}
}
