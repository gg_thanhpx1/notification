package vn.greenglobal.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import vn.tcx.framework.domain.AbstractAuditingEntity;

@Entity
@Table(name = "thong_tin_url")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ThongTinUrl extends AbstractAuditingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_THONG_TIN_URL")
    @SequenceGenerator(name = "SEQ_THONG_TIN_URL", sequenceName = "SEQ_THONG_TIN_URL", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@Column(name = "key", nullable = false, columnDefinition = "nvarchar2(255)")
	private String key;	
    
	@Column(name = "url", columnDefinition = "nvarchar2(255)")
    private String url;    
	
	@Column(name = "noi_dung", nullable = true, columnDefinition = "nvarchar2(500)")
    private String noiDung; 
	
	@Column(name = "type", columnDefinition = "nvarchar2(50)")
    private String type; 
	
	@Column(name = "web", columnDefinition = "boolean default true")
	private boolean web;
	
	@Column(name = "mobile", columnDefinition = "boolean default false")
	private boolean mobile;
	
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isWeb() {
        return web;
    }

    public void setWeb(boolean web) {
        this.web = web;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile = mobile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
