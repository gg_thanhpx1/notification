package vn.greenglobal.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import vn.tcx.framework.domain.AbstractAuditingEntity;

@Entity
@Table(name = "device")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Device extends AbstractAuditingEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEVICE")
    @SequenceGenerator(name = "SEQ_DEVICE", sequenceName = "SEQ_DEVICE", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@Column(name = "token")
	private String token;
	
	@Column(name = "loai_thiet_bi")
	private String loaiThietBi;
	
	@Column(name = "thong_tin")
	private String thongTin;
		
	@Column(name = "chuc_danh_id")
	private Long chucDanhId;
	
	@Column(name = "time_bat_dau_ngung_nhan", columnDefinition = "timestamp")
	private Instant timeBatDauNgungNhan;
		
	@Column(name = "time_ket_thuc_ngung_nhan", columnDefinition = "timestamp")
	private Instant timeKetThucNgungNhan;
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}

	public String getLoaiThietBi() {
		return loaiThietBi;
	}

	public void setLoaiThietBi(String loaiThietBi) {
		this.loaiThietBi = loaiThietBi;
	}

	public String getThongTin() {
		return thongTin;
	}

	public void setThongTin(String thongTin) {
		this.thongTin = thongTin;
	}

    public Long getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }

    public Instant getTimeBatDauNgungNhan() {
        return timeBatDauNgungNhan;
    }

    public void setTimeBatDauNgungNhan(Instant timeBatDauNgungNhan) {
        this.timeBatDauNgungNhan = timeBatDauNgungNhan;
    }

    public Instant getTimeKetThucNgungNhan() {
        return timeKetThucNgungNhan;
    }

    public void setTimeKetThucNgungNhan(Instant timeKetThucNgungNhan) {
        this.timeKetThucNgungNhan = timeKetThucNgungNhan;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
