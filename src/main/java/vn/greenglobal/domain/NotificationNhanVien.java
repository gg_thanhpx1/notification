package vn.greenglobal.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "notification_nhanvien")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NotificationNhanVien implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_NOTIFICATION_NHANVIEN")
    @SequenceGenerator(name = "SEQ_NOTIFICATION_NHANVIEN", sequenceName = "SEQ_NOTIFICATION_NHANVIEN", initialValue = 1, allocationSize = 1)
	private Long id;
	
	@Column(name = "notification_id")
	private Long notificationId;
	
	@Column(name = "chuc_danh_id")
	private Long chucDanhId;
	
	@Column(name = "da_doc")
	private boolean daDoc;

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

    public Long getChucDanhId() {
        return chucDanhId;
    }

    public void setChucDanhId(Long chucDanhId) {
        this.chucDanhId = chucDanhId;
    }

    public boolean isDaDoc() {
        return daDoc;
    }

    public void setDaDoc(boolean daDoc) {
        this.daDoc = daDoc;
    }
}
