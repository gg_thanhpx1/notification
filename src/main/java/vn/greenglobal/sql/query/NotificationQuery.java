package vn.greenglobal.sql.query;

import org.apache.commons.lang3.StringUtils;

public class NotificationQuery {

	public static final String FIND_NOTIFICATIONS_BY_CHUC_DANH_ID = "select new vn.greenglobal.dto.NotificationDTO(n.id, n.noiDung,"
            + StringUtils.SPACE + "n.url, nn.daDoc, n.ngayTao, n.type, n.mainId, n.key, n.secondId)"
	        + StringUtils.SPACE + "FROM Notification n"
	        + StringUtils.SPACE	+ "INNER JOIN NotificationNhanVien nn ON n.id = nn.notificationId"
	        + StringUtils.SPACE	+ "WHERE nn.chucDanhId = :chucDanhId AND n.daXoa = 0"
	        + StringUtils.SPACE	+ "ORDER BY n.ngayTao DESC";
	
	public static final String FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_AND_DA_DOC = "select new vn.greenglobal.dto.NotificationDTO(n.id, n.noiDung, "
            + StringUtils.SPACE + "n.url, nn.daDoc, n.ngayTao, n.type, n.mainId, n.key, n.secondId)"
	        + StringUtils.SPACE + "FROM Notification n"
	        + StringUtils.SPACE + "INNER JOIN NotificationNhanVien nn on n.id = nn.notificationId"
	        + StringUtils.SPACE + "WHERE nn.chucDanhId = :chucDanhId AND nn.daDoc = :daDoc AND n.daXoa = 0"
            + StringUtils.SPACE + "ORDER BY n.ngayTao DESC";
	
	public static final String FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_MOBILE = "select new vn.greenglobal.dto.NotificationDTO(n.id, n.noiDung, "
            + StringUtils.SPACE + "n.url, nn.daDoc, n.ngayTao, n.type, n.mainId, n.key, n.secondId)"
            + StringUtils.SPACE + "FROM Notification n"
            + StringUtils.SPACE + "INNER JOIN NotificationNhanVien nn ON n.id = nn.notificationId"
            + StringUtils.SPACE + "WHERE nn.chucDanhId = :chucDanhId AND n.daXoa = 0 AND n.mobile = 1"
            + StringUtils.SPACE + "ORDER BY n.ngayTao DESC";
    
    public static final String FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_AND_DA_DOC_MOBILE = "select new vn.greenglobal.dto.NotificationDTO(n.id, n.noiDung, "
            + StringUtils.SPACE + "n.url, nn.daDoc, n.ngayTao, n.type, n.mainId, n.key, n.secondId)"
            + StringUtils.SPACE + "FROM Notification n"
            + StringUtils.SPACE + "INNER JOIN NotificationNhanVien nn on n.id = nn.notificationId"
            + StringUtils.SPACE + "WHERE nn.chucDanhId = :chucDanhId AND nn.daDoc = :daDoc AND n.daXoa = 0 AND n.mobile = 1"
            + StringUtils.SPACE + "ORDER BY n.ngayTao DESC";
    
    public static final String FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_WEB = "select new vn.greenglobal.dto.NotificationDTO(n.id, n.noiDung, "
            + StringUtils.SPACE + "n.url, nn.daDoc, n.ngayTao, n.type, n.mainId, n.key, n.secondId)"
            + StringUtils.SPACE + "FROM Notification n"
            + StringUtils.SPACE + "INNER JOIN NotificationNhanVien nn ON n.id = nn.notificationId"
            + StringUtils.SPACE + "WHERE nn.chucDanhId = :chucDanhId AND n.daXoa = 0 AND n.web = 1"
            + StringUtils.SPACE + "ORDER BY n.ngayTao DESC";
    
    public static final String FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_AND_DA_DOC_WEB = "select new vn.greenglobal.dto.NotificationDTO(n.id, n.noiDung, "
            + StringUtils.SPACE + "n.url, nn.daDoc, n.ngayTao, n.type, n.mainId, n.key, n.secondId)"
            + StringUtils.SPACE + "FROM Notification n"
            + StringUtils.SPACE + "INNER JOIN NotificationNhanVien nn on n.id = nn.notificationId"
            + StringUtils.SPACE + "WHERE nn.chucDanhId = :chucDanhId AND nn.daDoc = :daDoc AND n.daXoa = 0 AND n.web = 1"
            + StringUtils.SPACE + "ORDER BY n.ngayTao DESC";
	
}
