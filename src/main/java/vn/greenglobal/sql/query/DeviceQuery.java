package vn.greenglobal.sql.query;

import org.apache.commons.lang3.StringUtils;

public class DeviceQuery {
    
	public static final String FIND_DEVICES_BY_CHUC_DANH_ID = "select new vn.greenglobal.dto.DeviceDTO("
	        + StringUtils.SPACE + "d.id, d.token, d.chucDanhId, d.loaiThietBi)"
	        + StringUtils.SPACE + "from Device d"
	        + StringUtils.SPACE + "where d.chucDanhId = :chucDanhId";
}
