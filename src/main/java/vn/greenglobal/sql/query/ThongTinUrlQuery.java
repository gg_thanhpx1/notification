package vn.greenglobal.sql.query;

import org.apache.commons.lang3.StringUtils;

public class ThongTinUrlQuery {

	public static final String FIND_THONG_TIN_URL = "select new vn.greenglobal.dto.ThongTinUrlDto(ttu.id, ttu.noiDung, "
	        + StringUtils.SPACE + "ttu.url, ttu.key, ttu.type, ttu.web, ttu.mobile)"
	        + StringUtils.SPACE + "from ThongTinUrl ttu"
	        + StringUtils.SPACE	+ "where ttu.daXoa = 0";
	
	public static final String FIND_THONG_TIN_URL_BY_KEY = "select new vn.greenglobal.dto.ThongTinUrlDto(ttu.id, ttu.noiDung, "
	        + StringUtils.SPACE + "ttu.url, ttu.key, ttu.type, ttu.web, ttu.mobile) "
	        + StringUtils.SPACE + "from ThongTinUrl ttu "
	        + StringUtils.SPACE + "where ttu.key = :key AND ttu.daXoa = 0";
}
