package vn.greenglobal.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.greenglobal.domain.ThongTinUrl;
import vn.greenglobal.dto.ThongTinUrlDto;
import vn.greenglobal.sql.query.ThongTinUrlQuery;

@Repository
public interface ThongTinUrlRepository extends JpaRepository<ThongTinUrl, Long> {
    Optional<ThongTinUrl> findByKeyAndDaXoaFalse(String key);
    
    @Query(value = ThongTinUrlQuery.FIND_THONG_TIN_URL)
    List<ThongTinUrlDto> listThongTinUrl(Pageable pageable);
    
    @Query(value = ThongTinUrlQuery.FIND_THONG_TIN_URL_BY_KEY)
    Optional<ThongTinUrlDto> findByKey(@Param("key") String key);
}
