package vn.greenglobal.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.greenglobal.domain.Notification;
import vn.greenglobal.dto.NotificationDTO;
import vn.greenglobal.sql.query.NotificationQuery;

@Repository
public interface NotificationRepository extends PagingAndSortingRepository<Notification, Long> {
	@Query(value = NotificationQuery.FIND_NOTIFICATIONS_BY_CHUC_DANH_ID)
	Page<NotificationDTO> findNotificationsByChucDanhId(@Param("chucDanhId") long chucDanhId, Pageable pageable);
	
	@Query(value = NotificationQuery.FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_AND_DA_DOC)
	Page<NotificationDTO> findNotificationsByChucDanhIdAndDaDoc(@Param("chucDanhId") long chucDanhId, @Param("daDoc") boolean daDoc, Pageable pageable);
	
	@Query(value = NotificationQuery.FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_WEB)
    Page<NotificationDTO> findNotificationsByChucDanhIdWeb(@Param("chucDanhId") long chucDanhId, Pageable pageable);
    
    @Query(value = NotificationQuery.FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_AND_DA_DOC_WEB)
    Page<NotificationDTO> findNotificationsByChucDanhIdAndDaDocWeb(@Param("chucDanhId") long chucDanhId, @Param("daDoc") boolean daDoc, Pageable pageable);
    
    @Query(value = NotificationQuery.FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_MOBILE)
    Page<NotificationDTO> findNotificationsByChucDanhIdMobile(@Param("chucDanhId") long chucDanhId, Pageable pageable);
    
    @Query(value = NotificationQuery.FIND_NOTIFICATIONS_BY_CHUC_DANH_ID_AND_DA_DOC_MOBILE)
    Page<NotificationDTO> findNotificationsByChucDanhIdAndDaDocMobile(@Param("chucDanhId") long chucDanhId, @Param("daDoc") boolean daDoc, Pageable pageable);
}
