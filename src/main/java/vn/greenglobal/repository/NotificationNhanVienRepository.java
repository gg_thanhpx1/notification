package vn.greenglobal.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.greenglobal.domain.NotificationNhanVien;

@Repository
public interface NotificationNhanVienRepository extends JpaRepository<NotificationNhanVien, Long> {
	// @Modifying
    // @Query(value = NotificationNhanVienQuery.DA_DOC_NOTIFICATION, nativeQuery = true)
	// void updateDaDocNotification(@Param("notificationId") Long notificationId,
	// 							 @Param("nhanVienId") Long nhanVienId);

	Optional<NotificationNhanVien> findByNotificationIdAndChucDanhId(Long notificationId, Long chucDanhId);
}
