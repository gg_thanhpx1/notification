package vn.greenglobal.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.greenglobal.domain.Device;
import vn.greenglobal.dto.DeviceDTO;
import vn.greenglobal.sql.query.DeviceQuery;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
	@Query(value = DeviceQuery.FIND_DEVICES_BY_CHUC_DANH_ID)
	List<DeviceDTO> findDeviceByChucDanhId(@Param("chucDanhId") long chucDanhId);
	
	List<Device> findByChucDanhId(long chucDanhId);

	Optional<Device> findByTokenAndDaXoaFalse(String token);
	
	Optional<Device> findByChucDanhIdAndLoaiThietBiAndThongTin(long chucDanhId, String loaiThietBi, String thongTin);

	@Modifying
    @Transactional
	void deleteByChucDanhIdAndToken(long chucDanhId, String token);
}
