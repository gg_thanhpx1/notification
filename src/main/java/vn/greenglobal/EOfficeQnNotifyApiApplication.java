package vn.greenglobal;

import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import feign.RequestInterceptor;
import vn.greenglobal.config.UserFeignClientInterceptor;
import vn.tcx.framework.config.ApplicationUtil;
import vn.tcx.framework.config.DefaultProfileUtil;
import vn.tcx.framework.config.TcxConstants;

@SpringBootApplication
public class EOfficeQnNotifyApiApplication extends SpringBootServletInitializer{
	
    private static final Logger log = LoggerFactory.getLogger(EOfficeQnNotifyApiApplication.class);
    
    @Autowired
    private Environment env;    
    
    public static void main(String[] args) {	
		SpringApplication app = new SpringApplication(EOfficeQnNotifyApiApplication.class);
        DefaultProfileUtil.addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        ApplicationUtil.logApplicationStartup(env, log);
	}
	
	@PostConstruct
    public void initApplication() {
        Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
        if (activeProfiles.contains(TcxConstants.SPRING_PROFILE_DEVELOPMENT)
                && activeProfiles.contains(TcxConstants.SPRING_PROFILE_PRODUCTION)) {
            log.error("You have misconfigured your application! It should not run "
                    + "with both the 'dev' and 'prod' profiles at the same time.");
        }
        if (activeProfiles.contains(TcxConstants.SPRING_PROFILE_DEVELOPMENT)
                && activeProfiles.contains(TcxConstants.SPRING_PROFILE_CLOUD)) {
            log.error("You have misconfigured your application! It should not "
                    + "run with both the 'dev' and 'cloud' profiles at the same time.");
        }
    }

	@Override
    public SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        /**
         * set a default to use when no profile is configured.
         */
        DefaultProfileUtil.addDefaultProfile(application.application());
        return application.sources(EOfficeQnNotifyApiApplication.class);
    }
	
	@Bean
    public RequestInterceptor getUserFeignClientInterceptor() {
        return new UserFeignClientInterceptor();
    }
}