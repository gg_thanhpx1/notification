package vn.greenglobal.enums;

public enum ApiMessageEnum {
	
	OK("Success Request"),
	NO_CONTENT("No Content"),
	NOT_FOUND("Not found"),
	BAD_REQUEST("Bad Request"),
	UNAUTHORIZED("Unauthorized"),
	INTERNAL_SERVER_ERROR("Internal Server Error");
	
	ApiMessageEnum(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	private String text;
}
