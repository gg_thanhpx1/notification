package vn.greenglobal.enums;

public enum NotificationsStatusEnum {
	
	ALL("ALL"),
	READ("READ"),
	UNREAD("UNREAD");
	
	NotificationsStatusEnum(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	private String text;
}
