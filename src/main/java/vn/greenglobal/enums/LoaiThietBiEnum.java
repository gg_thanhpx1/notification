package vn.greenglobal.enums;

public enum LoaiThietBiEnum {
	
	WEB("Web"),
	ANDROID("Android"),
	IOS("iOS");
	
    LoaiThietBiEnum(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	private String text;
}
