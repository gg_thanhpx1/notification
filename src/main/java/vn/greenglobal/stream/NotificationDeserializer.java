package vn.greenglobal.stream;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class NotificationDeserializer implements Deserializer<Notification> {

    @Override
    public Notification deserialize(String arg0, byte[] devBytes) {
        ObjectMapper mapper = new ObjectMapper();
        Notification notification = null;
        try {
            notification = mapper.readValue(devBytes, Notification.class);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return notification;
    }

    @Override
    public void close() {
        // TODO Auto-generated method stub

    }

    @Override
    public void configure(Map<String, ?> arg0, boolean arg1) {
        // TODO Auto-generated method stub

    }

}
