package vn.greenglobal.stream;

import java.util.List;

public class Notification {
	private String noiDung;

	private String key;
	
	private Object[] params;

	private List<Long> nguoiNhanIds;

	/**
	 * @return the noiDung
	 */
	public String getNoiDung() {
		return noiDung;
	}

	public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    /**
	 * @param noiDung the noiDung to set
	 */
	public void setNoiDung(String noiDung) {
		this.noiDung = noiDung;
	}

    public List<Long> getNguoiNhanIds() {
        return nguoiNhanIds;
    }

    public void setNguoiNhanIds(List<Long> nguoiNhanIds) {
        this.nguoiNhanIds = nguoiNhanIds;
    }
}
