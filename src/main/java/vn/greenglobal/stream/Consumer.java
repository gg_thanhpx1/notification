package vn.greenglobal.stream;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import vn.greenglobal.model.request.NotificationWithParamRequest;
import vn.greenglobal.service.PushNotificationService;

@Service
public class Consumer {
    
    
    @Autowired
    private PushNotificationService pushNotificationsService;
    //#{'${spring.kafka.consumer.topics}'}"
    @KafkaListener(topics = "#{'${spring.kafka.consumer.topics}'}", groupId = "group_id")
    public void consume(Notification message) throws IOException {
        System.out.println("===consume from notify:     " + message.getNoiDung());
        
        NotificationWithParamRequest request = new NotificationWithParamRequest();
        request.setKey(message.getKey());
        request.setNoiDung(message.getNoiDung());
        request.setParams(message.getParams());
        request.setNguoiNhanIds(message.getNguoiNhanIds());
        pushNotificationsService.sendNotificationWithParamToClient(request);
    }
}
